# Bitbucket Pipelines Pipe: McAfee MVISION Cloud Shift Left Inline Integration

This pipe perform the MVISION Cloud&#39;s Shift Left Integration with Bitbucket

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - git log HEAD -1 --name-only --pretty="format:" > changes.txt
  - egrep -i '.json|.tf|.yml|.yaml' changes.txt > changes_latest.txt
  - cat changes_latest.txt
  - data=$(cat changes_latest.txt | while read line; do echo $line; done)
  - data=$(echo $data  | tr ' ' ',')
  - pipe: mcafeemvision/shift-left-atlassian-pipe:1.1.0
  variables:
    MVISION_USERNAME: $MVISION_USERNAME
    MVISION_PASSWORD: $MVISION_PASSWORD
    MVISION_ENVIRONMENT: $MVISION_ENVIRONMENT
    CLOUD_SERVICE_PROVIDER: $CLOUD_SERVICE_PROVIDER
    FILES: $data
    WORKSPACE: $BITBUCKET_WORKSPACE
    SLUG: $BITBUCKET_REPO_SLUG
    COMMIT_ID: $BITBUCKET_COMMIT
    
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| MVISION_USERNAME (*)  | The user name of McAfee MVISION Cloud User |
| MVISION_PASSWORD (*)  | The password of McAfee MVISION Cloud User |
| MVISION_ENVIRONMENT (*)| The environment URL(usually https://www.myshn.net) of McAfee MVISION Cloud |
| $CLOUD_SERVICE_PROVIDER (*) |  Cloud service provider for which the templates needs to be evaluated(e.g., aws or azure)  |
| FILES (*)             | Files that need to submitted for evaluation | 

_(*) = required variable._

## Prerequisites
Create the variables MVISION_USERNAME, MVISION_PASSWORD, MVISION_ENVIRONMENT & CLOUD_SERVICE_PROVIDER in repository variables

## Examples

Basic example:

```yaml
script:
  - git log HEAD -1 --name-only --pretty="format:" > changes.txt
  - egrep -i '.json|.tf|.yml|.yaml' changes.txt > changes_latest.txt
  - cat changes_latest.txt
  - data=$(cat changes_latest.txt | while read line; do echo $line; done)
  - data=$(echo $data  | tr ' ' ',')
  - pipe: mcafeemvision/shift-left-atlassian-pipe:1.1.0
  variables:
    MVISION_USERNAME: $MVISION_USERNAME
    MVISION_PASSWORD: $MVISION_PASSWORD
    MVISION_ENVIRONMENT: $MVISION_ENVIRONMENT
    CLOUD_SERVICE_PROVIDER: $CLOUD_SERVICE_PROVIDER
    FILES: $data
    WORKSPACE: $BITBUCKET_WORKSPACE
    SLUG: $BITBUCKET_REPO_SLUG
    COMMIT_ID: $BITBUCKET_COMMIT
```

Advanced example:

```yaml
script:
  - git log HEAD -1 --name-only --pretty="format:" > changes.txt
  - egrep -i '.json|.tf|.yml|.yaml' changes.txt > changes_latest.txt
  - cat changes_latest.txt
  - data=$(cat changes_latest.txt | while read line; do echo $line; done)
  - data=$(echo $data  | tr ' ' ',')
  - pipe: mcafeemvision/shift-left-atlassian-pipe:1.1.0
  variables:
    MVISION_USERNAME: $MVISION_USERNAME
    MVISION_PASSWORD: $MVISION_PASSWORD
    MVISION_ENVIRONMENT: $MVISION_ENVIRONMENT
    CLOUD_SERVICE_PROVIDER: $CLOUD_SERVICE_PROVIDER
    FILES: $data
    WORKSPACE: $BITBUCKET_WORKSPACE
    SLUG: $BITBUCKET_REPO_SLUG
    COMMIT_ID: $BITBUCKET_COMMIT
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by krishna_c@mcafee.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
