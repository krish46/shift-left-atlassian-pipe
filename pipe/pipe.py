import requests
import sys
import json
import time

from bitbucket_pipes_toolkit import Pipe, get_logger

logger = get_logger()
schema = {
    'MVISION_USERNAME': {'type': 'string', 'required': True},
    'MVISION_PASSWORD': {'type': 'string', 'required': True},
    'MVISION_ENVIRONMENT': {'type': 'string', 'required': True},
    'CLOUD_SERVICE_PROVIDER': {'type': 'string', 'required': True},
    'FILES': {'type': 'string', 'required': True},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False},
    'WORKSPACE': {'type': 'string', 'required': True},
    'SLUG': {'type': 'string', 'required': True},
    'COMMIT_ID': {'type': 'string', 'required': True}
}

# Report Constants
REPORT_TYPE = "SECURITY"
REPORT_TITLE = "McAfee MVISION Cloud Shift Left Evaluation Report"
REPORT_DETAIL = "This report provides the details of Security Violations found in DevOps templates"
REPORT_NUMBER_OF_FILES = "Number of Files"
REPORT_DATA_TYPE = "NUMBER"
REPORT_EXTERNAL_ID = "mvision-{0}"
REPORT_URL = "http://api.bitbucket.org/2.0/repositories/{0}/{1}/commit/{2}/reports/{3}"
TYPE = "report"

# Annotation Constants
ANNOTATION_URL = "http://api.bitbucket.org/2.0/repositories/{0}/{1}/commit/{2}/reports/{3}/annotations/{4}"
ANNOTATION_TYPE = "VULNERABILITY"
ANNOTATION_FAILED_RESULT = "FAILED"
ANNOTATION_EXTERNAL_ID = "annotation-{0}"
ANNOTATION_TYPE = "VULNERABILITY"

# MVISION Constants
AUTH_URL = "{0}/neo/neo-auth-service/oauth/token?grant_type=password"
FAILURE = "Failure"
STATUS = 'status'
SUBMITTED = "Submitted"
IN_PROGRESS = "In Progress"
MESSAGE = 'message'
VIOLATION_COUNT = 'violation_count'
NO_VIOLATION_MESSAGE = "No violations were found."

PROXY_URL = "http://host.docker.internal:29418"
PIPELINE_SPEC_FILE = "bitbucket-pipelines.yml"


class ShiftLeftPipe(Pipe):
    def run(self):
        super().run()
        logger.info('Executing the pipe...')
        username = self.get_variable('MVISION_USERNAME')
        password = self.get_variable('MVISION_PASSWORD')
        environment = self.get_variable('MVISION_ENVIRONMENT')
        csp = self.get_variable('CLOUD_SERVICE_PROVIDER')
        token = self.get_access_token(username, password, environment)
        files = str(self.get_variable('FILES')).split(",")
        if len(files) == 0:
            logger.info("No files to evaluate")
            self.success("No files to evaluate")
        violated_files = []
        run_time = int(round(time.time() * 1000))
        external_id = str.format(REPORT_EXTERNAL_ID, run_time)
        self.create_report(external_id, len(files))
        for file in files:
            if file == PIPELINE_SPEC_FILE:
                logger.info("ignoring build spec file.")
                continue
            response = self.submit_file_for_scan(username, password, environment, csp, token, file)
            status = response[STATUS]
            message = response[MESSAGE]
            if status == FAILURE:
                logger.error(message)
                continue
            while 1:
                time.sleep(30)
                logger.info("Checking the status for the file: %s", file)
                headers = {
                    'Content-Type': 'application/json',
                    'x-access-token': token
                }
                status_response = requests.get(message, headers=headers)
                url = message.split("/", -1)
                uuid = url[len(url) - 1]
                annotation_id = str.format(ANNOTATION_EXTERNAL_ID, uuid)
                if status_response.status_code == 401:
                    logger.debug("Token has expired. Get new access token for the user: %s", username)
                    token = self.get_access_token(username, password)
                    continue
                elif status_response.status_code == 200:
                    scan_eval_response = status_response.json()
                    if scan_eval_response[STATUS] == SUBMITTED or scan_eval_response[STATUS] == IN_PROGRESS:
                        logger.info(scan_eval_response[MESSAGE])
                        continue
                    elif scan_eval_response[STATUS] == FAILURE:
                        break
                    else:
                        message = scan_eval_response[MESSAGE]
                        if message[VIOLATION_COUNT] > 0:
                            violated_files.append(file)
                            details = str.format("{0} violations were found for the file: {1}.",
                                                 message[VIOLATION_COUNT], file)
                            value = str.format("Violated the policies: {0}", message['policies_violated'])
                            self.create_annotation(external_id, annotation_id, file, details, value)
                            logger.critical(details + value)
                            break
                        elif message[VIOLATION_COUNT] == 0:
                            details = str.format("No violations were found for the file: {0}", file)
                            self.create_annotation(external_id, annotation_id, file, details, None)
                            logger.info(details)
                            break
        if len(violated_files) > 0:
            self.fail(
                str.format("Failing the build, since violations were found for the files: {0}.", str(violated_files)))
        self.success(NO_VIOLATION_MESSAGE)

    def create_report(self, external_id, number_of_files):
        http_proxy = PROXY_URL
        workspace = self.get_variable('WORKSPACE')
        slug = self.get_variable('SLUG')
        commit_id = self.get_variable('COMMIT_ID')
        proxyDict = {
            "http": http_proxy
        }
        url = str.format(REPORT_URL, workspace, slug, commit_id, external_id)
        headers = {
            'Content-Type': 'application/json'
        }
        data = {
            "report_type": REPORT_TYPE,
            "external_id": external_id,
            "title": REPORT_TITLE,
            "details": REPORT_DETAIL,
            "data": [
                {
                    "title": REPORT_NUMBER_OF_FILES,
                    "type": REPORT_DATA_TYPE,
                    "value": number_of_files
                }
            ]
        }
        response = requests.put(url, data=json.dumps(data), headers=headers, proxies=proxyDict)
        logger.info(response.status_code)
        logger.debug(response.content)

    def create_annotation(self, report_id, annotation_id, file_path, details, value):
        http_proxy = PROXY_URL
        work_space = self.get_variable('WORKSPACE')
        slug = self.get_variable('SLUG')
        commit_id = self.get_variable('COMMIT_ID')
        proxy_dict = {
            "http": http_proxy
        }
        url = str.format(ANNOTATION_URL, work_space, slug, commit_id, report_id, annotation_id)
        # logger.info(url)
        headers = {
            'Content-Type': 'application/json'
        }
        data = {
            "annotation_type": ANNOTATION_TYPE,
            "external_id": annotation_id,
            "line": 1,
            "path": file_path,
            "summary": details,
            "details": value,
            "result": ANNOTATION_FAILED_RESULT
        }
        response = requests.put(url, data=json.dumps(data), headers=headers, proxies=proxy_dict)
        logger.debug(response.content)

    def get_access_token(self, username, password, environment):
        url = str.format(AUTH_URL, environment)
        headers = {
            'Content-Type': 'application/json',
            'x-auth-username': username,
            'x-auth-password': password
        }
        payload = {}
        response = requests.request("POST", url, headers=headers, data=payload)

        if response.status_code != 200:
            logger.critical("Unable to receive access token for user: %s", username)
            sys.exit(response)
        logger.info("Successfully received the access token for user: %s", username)
        data = response.json()
        return data['access_token']

    def submit_file_for_scan(self, username, password, environment, csp, token, filename, retry_count=0):
        url = str.format("{0}/neo/config-audit/devops/v1/scan?service={1}", environment, csp)

        headers = {
            'x-access-token': token
        }
        payload = {}
        files = [
            ('templateFile', open(filename, 'rb'))
        ]
        response = requests.request("POST", url, headers=headers, data=payload, files=files)
        if response.status_code == 401:
            logger.debug("Token has expired. Get new access token for the user: %s", username)
            token = self.get_access_token(username, password)
            self.submit_file_for_scan(username, password, environment, csp, token, filename)
        elif response.status_code == 200:
            logger.info("Successfully submitted the file: %s for evaluation", filename)
            return response.json()
        elif response.status_code == 429:
            logger.info(
                "Unable to submit the file: %s due to too many request. The request will be submitted "
                "again in sometime.",
                filename)
            time.sleep(60)
            retry_count += 1
            if retry_count <= 5:
                self.submit_file_for_scan(username, password, environment, csp, token, filename, retry_count)
            else:
                logger.critical("Too many requests being processed. Unable to submit the file: %s", filename)
        else:
            logger.error("Error while submitting the file: %s due to response: %s", filename, response.text)


if __name__ == '__main__':
    pipe = ShiftLeftPipe(pipe_metadata='/pipe.yml', schema=schema)
    pipe.run()
